package com.sj.motion;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import com.sj.motion.plugins.Click;
import com.sj.motion.plugins.Nav;
import com.sj.motion.plugins.Scroll;
import com.sj.motion.plugins.StatePlugin;

import java.util.ArrayList;
import java.util.List;

import static com.sj.motion.MotionUtil.buildPacket;
import static com.sj.proto.Motion.MotionPacket;
import static com.sj.proto.Motion.MotionType;

/**
 * Created by IntelliJ IDEA.
 * User: sam
 * Date: 9/8/12
 * Time: 12:18 PM
 * To change this template use File | Settings | File Templates.
 */

public class StateCoordinator implements View.OnTouchListener {
    private static final int CLICK_DELAY = 125;
    private List<StatePlugin> plugins = new ArrayList<StatePlugin>();
    private State currentState = State.REST;

    private long lastClick = 0;

    private final Handler packetHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            MotionPacket packet = (MotionPacket) message.obj;
            if (packet.getType() == MotionType.CLICK){
                sendPacket(packet, CLICK_DELAY);
                lastClick = System.currentTimeMillis();
            } else {
                sendPacket(packet);
            }
            return true;
        }
    });

    private Handler sendHandler;

    public StateCoordinator(Handler sendHandler){
        this.sendHandler = sendHandler;
        plugins.add(new Click(packetHandler));
        plugins.add(new Nav(packetHandler));
        plugins.add(new Scroll(packetHandler));
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        final double x = motionEvent.getX();
        final double y = motionEvent.getY();
        final int fingers = motionEvent.getPointerCount();

        final Action action = getAction(motionEvent);

        if (action != null){
            for (StatePlugin plugin : plugins){
                if (plugin.handlesState(currentState, action)){
                    plugin.handleAction(x, y, fingers);
                }
            }
        }

        final State newState = getNewState(action, fingers);
        if (currentState != newState){
            Log.d("state", "change old=" + currentState + " new=" + newState);
        }
        currentState = newState;

        return true;
    }

    private Action getAction(MotionEvent motionEvent){
        final Action action;
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_POINTER_2_DOWN:
                action = Action.FINGER_DOWN;
                Log.d("touch", "second finger down");
                break;
            case MotionEvent.ACTION_POINTER_2_UP:
                action = Action.FINGER_UP;
                Log.d("touch", "second finger up");
                break;
            case MotionEvent.ACTION_POINTER_1_UP:
                action = Action.FINGER_UP;
                Log.d("touch", "first finger up");
                break;
            case MotionEvent.ACTION_DOWN:
                action = Action.FINGER_DOWN;
                break;
            case MotionEvent.ACTION_UP:
                action = Action.FINGER_UP;
                break;
            case MotionEvent.ACTION_MOVE:
                action = Action.MOVE;
                break;
            default: action = null;
        }
        return action;
    }

    private State getNewState(Action action, int fingers){
        State newState = currentState;
        if (action == Action.FINGER_DOWN){
            switch (currentState){
                case REST:
                    if (System.currentTimeMillis() - lastClick < CLICK_DELAY){
                        newState = State.DRAG;
                        sendHandler.removeMessages(0);
                        sendPacket(MotionType.DRAG);
                    } else {
                        newState = State.NAV;
                    }
                    break;
                case NAV:
                    newState = State.SCROLL;
                    break;
            }
        } else if (action == Action.FINGER_UP){
            switch (currentState){
                case SCROLL:
                    if (fingers == 1){
                        newState = State.REST;
                    }
                    break;
                case NAV:
                    newState = State.REST;
                    break;
                case DRAG:
                    newState= State.REST;
                    sendPacket(MotionType.RELEASE);
                    break;
            }
        }
        return newState;
    }

    private void sendPacket(MotionType type){
        sendPacket(buildPacket(type));
    }

    private void sendPacket(MotionPacket packet){
        sendHandler.obtainMessage(0, packet).sendToTarget();
    }

    private void sendPacket(MotionPacket packet, long delay){
        final Message message = sendHandler.obtainMessage(0, packet);
        sendHandler.sendMessageDelayed(message, delay);
    }
}
