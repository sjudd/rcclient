package com.sj.motion.plugins;

import android.os.Handler;
import com.sj.motion.Action;
import com.sj.motion.State;

import static com.sj.proto.Motion.MotionPacket;

/**
 * Created by IntelliJ IDEA.
 * User: sam
 * Date: 9/8/12
 * Time: 12:21 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class StatePlugin {
    private PluginAction nextAction;

    protected enum PluginAction {
        UPDATE_POSITION,
        PRIMARY;
    }

    private final Handler packetHandler;

    public StatePlugin(Handler packetHandler){
        this.packetHandler = packetHandler;
    }

    public void handleAction(double x, double y, int touchPoints){
        handleAction(x, y, touchPoints, nextAction);
    }

    public abstract void handleAction(double x, double y, int touchPoints, PluginAction action);

    public final boolean handlesState(State state, Action action) {
        nextAction = getPluginAction(state, action);
        return nextAction != null;
    }

    public abstract PluginAction getPluginAction(State state, Action action);

    protected void queuePacket(MotionPacket packet){
        packetHandler.obtainMessage(0, packet).sendToTarget();
    }
}
