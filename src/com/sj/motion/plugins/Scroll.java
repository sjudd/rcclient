package com.sj.motion.plugins;

import android.os.Handler;
import android.util.Log;
import com.sj.motion.Action;
import com.sj.motion.State;
import com.sj.proto.Motion;

import static com.sj.motion.MotionUtil.Point;
import static com.sj.motion.MotionUtil.buildPacket;
import static com.sj.proto.Motion.Direction;
import static com.sj.proto.Motion.MotionType;

/**
 * Created by IntelliJ IDEA.
 * User: sam
 * Date: 9/8/12
 * Time: 1:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class Scroll extends StatePlugin {
    private static final double MIN_SCROLL = 20;

    private Point position = new Point(0, 0);

    public Scroll(Handler packetHandler) {
        super(packetHandler);
    }

    @Override
    public void handleAction(double x, double y, int touchPoints, PluginAction action) {
        switch (action) {
            case UPDATE_POSITION:
                position.x = x;
                position.y = y;
                return;
            case PRIMARY:
                if (Math.abs(position.y - y) > MIN_SCROLL){
                    final Direction dir = position.y > y ? Direction.UP : Direction.DOWN;
                    Log.d("touch", "SCROLL dir=" + dir);
                    position.y = y;
                    queuePacket(buildPacket(MotionType.SCROLL, Motion.Scroll.newBuilder().setDirection(dir).build().toByteString()));
                }
                return;
        }
    }

    @Override
    public PluginAction getPluginAction(State state, Action action) {
        if (state == State.NAV && action == Action.FINGER_DOWN) {
            return PluginAction.UPDATE_POSITION;
        } else if (state == State.SCROLL && action == Action.MOVE) {
            return PluginAction.PRIMARY;
        } else {
            return null;
        }
    }
}
