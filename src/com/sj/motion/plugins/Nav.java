package com.sj.motion.plugins;

import android.os.Handler;
import android.util.Log;
import com.sj.motion.Action;
import com.sj.motion.State;
import com.sj.proto.Motion;
import com.sj.util.FixedQueue;

import java.util.List;

import static com.sj.motion.MotionUtil.Point;
import static com.sj.motion.MotionUtil.buildPacket;
import static com.sj.proto.Motion.Move;

/**
 * Created by IntelliJ IDEA.
 * User: sam
 * Date: 9/8/12
 * Time: 1:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class Nav extends StatePlugin {
    private static final int SMOOTH_POINTS = 3;
    private static final int SENSITIVITY = 10;
    private static final int ACCEL_DISTANCE = 20;
    private static final int ACCEL_BOUND_DX = 10;
    private static final double MIN_MOVE = 1;

    private Point position = new Point(0, 0);
    private FixedQueue lastPoints = new FixedQueue(SMOOTH_POINTS);
    private boolean shouldAccel = true;

    public Nav(Handler packetHandler) {
        super(packetHandler);
    }

    @Override
    public void handleAction(double x, double y, int touchPoints, PluginAction action) {
        switch (action) {
            case UPDATE_POSITION:
                position.x = x;
                position.y = y;
                return;
            case PRIMARY:
                final double preciseDx = position.x - x;
                final double preciseDy = position.y - y;
                final Point smoothedDxDy = getDxDy(new Point(preciseDx, preciseDy));
                final double sensitivity = getSensitivity(preciseDx, preciseDy);
                final int dx = (int) Math.round(smoothedDxDy.x * sensitivity);
                final int dy = (int) Math.round(smoothedDxDy.y * sensitivity);
                final boolean movedX = Math.abs(dx) >= MIN_MOVE;
                final boolean movedY = Math.abs(dy) >= MIN_MOVE;
                if (movedX || movedY){
                    Move.Builder builder = Move.newBuilder();
                    if (movedX){
                        position.x = x;
                        builder.setDx(dx);
                    } else {
                        builder.setDx(0);
                    }
                    if (movedY){
                        position.y = y;
                        builder.setDy(dy);
                    } else {
                        builder.setDy(0);
                    }
                    Log.d("touch", "MOVE dx: " + dx + " dy: " + dy);
                    queuePacket(buildPacket(Motion.MotionType.MOVE, builder.build().toByteString()));

                }
                return;
        }
    }

    @Override
    public PluginAction getPluginAction(State state, Action action) {
        if (state == State.REST && action == Action.FINGER_DOWN) {
            return PluginAction.UPDATE_POSITION;
        } else if ((state == State.NAV || state == State.DRAG) && action == Action.MOVE) {
            return PluginAction.PRIMARY;
        } else {
            return null;
        }
    }

    private Point getDxDy(Point newDxDy){
        lastPoints.add(newDxDy);
        double dx = 0;
        double dy = 0;
        final List<Point> items = lastPoints.getItems();
        final double[] weights;
        switch (items.size()) {
            case 2: weights = new double[]{0.6, 0.4}; break;
            case 3: weights = new double[]{0.6, 0.2, 0.2}; break;
            default: weights = new double[]{1};
        }

        for (int i = 0; i < items.size(); i++){
            final Point point = items.get(i);
            final double weight = weights[i];
            dx += (point.x * weight);
            dy += (point.y * weight);
        }
        Log.d("getDxDy", "found=" + items.size());
        return new Point(dx/items.size(), dy/items.size());
    }

    private double getSensitivity(double dx, double dy){
        if (shouldAccel){
            if (dx > ACCEL_BOUND_DX || dy > ACCEL_BOUND_DX){
                shouldAccel = false;
            } else {
                return SENSITIVITY * Math.max(.5, Math.min(1, Math.max(Math.abs(dx)/ACCEL_DISTANCE, Math.abs(dy)/ACCEL_DISTANCE)));
            }
        }
        return SENSITIVITY;
    }
}
