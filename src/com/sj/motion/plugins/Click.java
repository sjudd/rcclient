package com.sj.motion.plugins;

import android.os.Handler;
import android.util.Log;
import com.sj.motion.Action;
import com.sj.motion.State;
import com.sj.proto.Motion;

import static com.sj.motion.MotionUtil.TimedPoint;
import static com.sj.motion.MotionUtil.buildPacket;

/**
 * Created by IntelliJ IDEA.
 * User: sam
 * Date: 9/8/12
 * Time: 12:55 PM
 * To change this template use File | Settings | File Templates.
        */
public class Click extends StatePlugin {
    private static final int CLICK_TIME = 250;
    private TimedPoint downPos = new TimedPoint(0, 0, 0);

    public Click(Handler packetHandler) {
        super(packetHandler);
    }

    @Override
    public void handleAction(double x, double y, int touchPoints, PluginAction action) {
        if (touchPoints != 1){
            return;
        } else {
            switch (action) {
                case UPDATE_POSITION:
                    downPos.ts = System.currentTimeMillis();
                    downPos.x = x;
                    downPos.y = y;
                    return;
                case PRIMARY:
                    if (isClick(x, y)){
                        Log.d("touch", "CLICK");
                        queuePacket(buildPacket(Motion.MotionType.CLICK));
                    }
                    return;
            }
        }
    }

    @Override
    public PluginAction getPluginAction(State state, Action action) {
        if (state == State.REST && action == Action.FINGER_DOWN) {
            return PluginAction.UPDATE_POSITION;
        } else if  (state == State.NAV && action == Action.FINGER_UP) {
            return PluginAction.PRIMARY;
        } else {
            return null;
        }
    }

    private boolean isClick(double x, double y){
        return (System.currentTimeMillis() - downPos.ts < CLICK_TIME &&
                Math.abs(downPos.x - x) < 5 &&
                Math.abs(downPos.y - y) < 5 );
    }
}
