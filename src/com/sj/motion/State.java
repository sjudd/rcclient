package com.sj.motion;

/**
 * Created by IntelliJ IDEA.
 * User: sam
 * Date: 9/8/12
 * Time: 1:42 PM
 * To change this template use File | Settings | File Templates.
 */
public enum State {
    REST, //doing nothing
    NAV, //moving the cursor around
    DRAG, //moving the cursor with left mouse button held
    SCROLL, //moving the scroll wheel
}

