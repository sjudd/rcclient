package com.sj.motion;

import com.google.protobuf.ByteString;

import static com.sj.proto.Motion.MotionPacket;
import static com.sj.proto.Motion.MotionType;

/**
 * Created by IntelliJ IDEA.
 * User: sam
 * Date: 9/8/12
 * Time: 12:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class MotionUtil {
    public static MotionPacket buildPacket(MotionType type){
        return buildPacket(type, null);
    }

    public static MotionPacket buildPacket(MotionType type, ByteString data){
        MotionPacket.Builder builder = MotionPacket.newBuilder();
        builder.setType(type);
        if (data != null){
            builder.setPacket(data);
        }
        return builder.build();
    }

   public static class Point{
        public double x;
        public double y;
        public Point(double x, double y){
            this.x = x;
            this.y = y;
        }
    }

    public static class TimedPoint extends Point{
        public double ts;
        public TimedPoint(double x, double y, double ts){
            super(x, y);
            this.ts = ts;
        }

    }
}
