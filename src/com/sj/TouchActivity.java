package com.sj;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;
import com.sj.motion.StateCoordinator;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import static com.sj.proto.Motion.MotionPacket;


public class TouchActivity extends Activity
{
    private static final int BUFFER_SIZE = 48;
    private static final int SIZE_BYTE_LEN = 4;
    private static final String IP_ADDRESS = "192.168.1.66";
    private static final int SOCKET = 64352;

    private DatagramSocket datagramSocket;
    private WorkerThread sendThread;
    private InetAddress address;
    private TextParser textParser;
    private StateCoordinator motionParser;

    private Handler packetReadyHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            MotionPacket packet = (MotionPacket) message.obj;
            sendResult(packet);
            return true;
        }
    });

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        View showKeyboard = findViewById(R.id.show_keyboard);
        final EditText fakeEdit = (EditText) findViewById(R.id.fake_edit);
        textParser = new TextParser(packetReadyHandler);
        fakeEdit.addTextChangedListener(textParser);
        fakeEdit.setSelection(fakeEdit.getText().length());

        showKeyboard.setOnClickListener(new View.OnClickListener() {
            private boolean isShowing = false;

            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "show keyboard touch " + isShowing, Toast.LENGTH_SHORT).show();

                fakeEdit.setFocusableInTouchMode(true);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (isShowing) {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                } else {
                    fakeEdit.requestFocus();
                    imm.showSoftInput(fakeEdit, InputMethodManager.SHOW_FORCED);
                }
                isShowing = !isShowing;
            }
        });

        View touchArea = findViewById(R.id.test);
        motionParser = new StateCoordinator(packetReadyHandler);

        sendThread = new WorkerThread("send_thread");
        sendThread.start();

        touchArea.setOnTouchListener(motionParser);
    }

    @Override
    protected void onResume() {
        super.onResume();
        createSocket(IP_ADDRESS);
    }

    @Override
    protected void onPause() {
        super.onPause();
        destroySocket();
    }

    private void createSocket(String addressString){
         try {
            datagramSocket = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }

        try {
            address = InetAddress.getByName(addressString);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    private void destroySocket(){
        datagramSocket.close();
        datagramSocket = null;
        address = null;
    }

    private byte[] getSizeBytes(MotionPacket packet){
        ByteBuffer sizeBuff = ByteBuffer.allocate(SIZE_BYTE_LEN);
        sizeBuff.order(ByteOrder.BIG_ENDIAN);
        sizeBuff.putInt(packet.getSerializedSize());
        return sizeBuff.array();
    }

    private DatagramPacket getWirePacket(MotionPacket packet){
        byte[] buffer = new byte[BUFFER_SIZE];
        byte[] packetBytes = packet.toByteArray();
        byte[] sizeBytes = getSizeBytes(packet);

        assert packetBytes.length + sizeBytes.length < BUFFER_SIZE;

        for (int i = 0; i < SIZE_BYTE_LEN; i++){
            buffer[i] = sizeBytes[i];
        }
        for (int i = SIZE_BYTE_LEN; i < SIZE_BYTE_LEN + packetBytes.length; i++){
            buffer[i] = packetBytes[i-SIZE_BYTE_LEN];
        }

        return new DatagramPacket(buffer, BUFFER_SIZE, address, SOCKET);
    }

    private void sendResult(MotionPacket packet){
        Log.d("SEND", "type = " + packet.getType() + "size = " + packet.getSerializedSize() + " size bytes=" + new Integer(packet.getSerializedSize()).byteValue());

        DatagramPacket datagram = getWirePacket(packet);
        try {
            datagramSocket.send(datagram);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private String asHex(byte[] bytes){
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes){
            sb.append(String.format("%02X ", b));
        }
        return sb.toString();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();    //To change body of overridden methods use File | Settings | File Templates.
        sendThread.quit();
    }
}
