package com.sj;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import com.sj.util.TextUtils;

/**
 * Created by IntelliJ IDEA.
 * User: sam
 * Date: 8/29/12
 * Time: 12:30 AM
 * To change this template use File | Settings | File Templates.
 */
public class TextParser implements TextWatcher{
    private Handler packetHandler;
    private boolean lastCharIsPlaceholder = false;

    public TextParser(Handler packetHandler){
        this.packetHandler = packetHandler;

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
        if (charSequence.toString().equalsIgnoreCase(" ") && lastCharIsPlaceholder){
            lastCharIsPlaceholder = false;
            return;
        }
        Log.d("TEXT_CHANGED", "sequence=|" + charSequence + "| start=" + start + " before=" + before + " count=" + count);
        String key = null;
        if (count - before == 1){
            Character lastChar = charSequence.charAt(charSequence.length() - 1);
            if (lastChar == ' '){
                key = TextUtils.SPACE_SYM;
            } else {
                Log.d("TEXT", "last key=" + lastChar + " as int=" + ((int) lastChar));
                key = lastChar.toString();
            }
        } else if (count - before == -1) {
            key = TextUtils.DELETE_SYM;
        } else {
            Log.d("TEXT", "no key!");
        }
        if (key != null)
            packetHandler.obtainMessage(0, TextUtils.buildTextPacket(key)).sendToTarget();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
    @Override
    public void afterTextChanged(Editable editable) {
        if (editable.toString().length() == 0){
            lastCharIsPlaceholder = true;
            editable.append(" ");
        } else if (editable.toString().equalsIgnoreCase(" ")){
            return;
        }
    }
}
