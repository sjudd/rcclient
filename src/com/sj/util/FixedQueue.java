package com.sj.util;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: sam
 * Date: 9/8/12
 * Time: 12:53 AM
 * To change this template use File | Settings | File Templates.
 */
public class FixedQueue<E>{
    private final ArrayList<E> items;
    private final int size;
    private int current = 0;

    public FixedQueue(int size){
        this.size = size;
        items = new ArrayList<E>(size);
    }

    public void add(E o){
        if (items.size() < size){
            items.add(o);
        } else {
            items.set(current, o);
            current = (current + 1) % size;
        }
    }

    public void clear(){
        items.clear();
        current = 0;
    }

    public ArrayList<E> getItems(){
        ArrayList<E>out = new ArrayList<E>(items.size());
        for (int i = 0; i < items.size(); i++){
            out.add(0, items.get((current + i) % items.size()));
        }
        return out;
    }
}
