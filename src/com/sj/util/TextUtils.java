package com.sj.util;

import com.sj.proto.Motion;

/**
 * Created by IntelliJ IDEA.
 * User: sam
 * Date: 8/29/12
 * Time: 11:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class TextUtils {
    public static final String DELETE_SYM = "DELETE";
    public static final String ENTER_SYM = "ENTER";
    public static final String SPACE_SYM = "SPACE";
    public static final int DELETE_CODE = 67;
    public static final int ENTER_CODE = 66;
    public static final String SEPARATOR = "||";

    public static Motion.MotionPacket buildTextPacket(String string){
        String result = string;
        if (string.length() % 2 == 1){
            result = string + " ";
        }
        return Motion.MotionPacket.newBuilder().
                setType(Motion.MotionType.KEY).
                setPacket(Motion.KeyboardEvent.newBuilder().
                        setText(result).
                        build().
                        toByteString()).
                build();
    }
}
