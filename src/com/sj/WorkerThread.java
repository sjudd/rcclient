package com.sj;

import android.os.Handler;
import android.os.HandlerThread;

/**
 * Created by IntelliJ IDEA.
 * User: sam
 * Date: 8/27/12
 * Time: 4:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class WorkerThread extends HandlerThread {
    public WorkerThread(String name) {
        super(name);
    }

    public synchronized Handler getHandler(){
        return new Handler(getLooper());
    }

    public synchronized Handler getHandler(Handler.Callback callback){
        return new Handler(getLooper(), callback);
    }

}
